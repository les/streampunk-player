# Streampunk embeddable player

#### Based on **[Vuidget — How to create an embeddable Vue.js widget with vue-custom-element](https://medium.com/@info_53938/vuidget-how-to-create-an-embeddable-vue-js-widget-with-vue-custom-element-674bdcb96b97)** @

## Usage
This is a [customElement](https://developer.mozilla.org/en-US/docs/Web/API/Window/customElements) that allows you to embed an audio player as an HTML component:

```html
<streampunk-player
  src="https://stream.radioblackout.org/blackout.ogg">
</streampunk-player>
```


### The command `yarn build` will build your source files for the widget.


## Project setup
```
yarn
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

