import Vue from 'vue'
import Player from './Player.vue'

// import router from './router'
// import store from './store/index'
// import './assets/css/main.scss'

// Custom elements polyfill
import 'document-register-element/build/document-register-element'
import vueCustomElement from 'vue-custom-element'
Vue.use(vueCustomElement)

Vue.config.productionTip = false

// Player.store = store
// Player.router = router

// register a custom element using vue-custom-element
Vue.customElement('streampunk-player', Player)

// uncomment if you need a shadowDOM widget
// , {
//   shadow: true,
//   beforeCreateVueInstance (root) {
//     const rootNode = root.el.getRootNode()

//     if (rootNode instanceof ShadowRoot) {
//       root.shadowRoot = rootNode
//     } else {
//       root.shadowRoot = document.head
//     }
//     return root
//   }
// })
